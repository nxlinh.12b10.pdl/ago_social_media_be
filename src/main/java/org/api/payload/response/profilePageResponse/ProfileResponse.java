package org.api.payload.response.profilePageResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.api.payload.response.UserResponse.PostResponse;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProfileResponse implements Serializable {
    private ProfileTimeLine myPost;
    private ProfileUserResponse myProfile;
}
