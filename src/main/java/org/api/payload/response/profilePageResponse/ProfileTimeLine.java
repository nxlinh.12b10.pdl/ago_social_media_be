package org.api.payload.response.profilePageResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.api.payload.response.pageResponse.PageResponse;

public class ProfileTimeLine extends PageResponse<TimeLineResponse> {
}
